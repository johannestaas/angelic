#!/usr/bin/env python

import os
import sys
import argparse
from time import sleep

from angelic import Daemon

if not os.path.exists('./.angelic_example.cfg'):
    with open('./.angelic_example.cfg', 'w') as f:
        f.write('pid_path=./angelic_example.pid\n'
                'foo=bar\n')

daemon = Daemon('angelic_example', debug_log='~/angelic_example_debug.log')
daemon.log.info('Ran the daemon script')


@daemon.daemonize
def loop(debug=False):
    ct = 0
    while True:
        ct += 1
        if debug:
            sys.stderr.write('%d\t' % ct)
        daemon.log.info('Info logging, count is %d' % ct)
        daemon.log.debug('config value of foo is: %s' % daemon.config['foo'])
        sleep(1)


@daemon.stop
def its_over():
    daemon.log.info('I was killed! My PID was %s' % str(daemon.get_pid()))


@daemon.start
def its_begun(debug=False):
    daemon.log.info('It has begun. Debug mode is: %s' % debug)


parser = argparse.ArgumentParser()
subs = daemon.setup_args(parser)
subs['start'].add_argument('--debug', '-d', action='store_true')
subs['restart'].add_argument('--debug', '-d', action='store_true')
daemon.parse_args(parser)
